package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorDemo {
    public static void main(String[] args) {
        Food thickBunBurgerSpecial = BreadProducer.THICK_BUN
                .createBreadToBeFilled();
        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT
                .addFillingToBread(thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CHEESE
                .addFillingToBread(thickBunBurgerSpecial);

        System.out.println("ThickBun Burger Special with ingredients of : "
                + thickBunBurgerSpecial.getDescription() + " with the price of : $"
                + thickBunBurgerSpecial.cost());

        Food thinBunBurgerSpecial = BreadProducer.THIN_BUN
                .createBreadToBeFilled();
        thinBunBurgerSpecial = FillingDecorator.CHICKEN_MEAT
                .addFillingToBread(thinBunBurgerSpecial);
        thinBunBurgerSpecial = FillingDecorator.TOMATO
                .addFillingToBread(thinBunBurgerSpecial);

        System.out.println("ThinBun Burger Special with ingredients of : "
                + thinBunBurgerSpecial.getDescription() + " with the price of : $"
                + thinBunBurgerSpecial.cost());

        Food crustySandwichSpecial = BreadProducer.CRUSTY_SANDWICH
                .createBreadToBeFilled();
        crustySandwichSpecial = FillingDecorator.CHICKEN_MEAT
                .addFillingToBread(crustySandwichSpecial);
        crustySandwichSpecial = FillingDecorator.CUCUMBER
                .addFillingToBread(crustySandwichSpecial);

        System.out.println("Crusty Sandwich Special with ingredients of : "
                + crustySandwichSpecial.getDescription() + " with the price of : $"
                + crustySandwichSpecial.cost());

        Food noCrustSandwichSpecial = BreadProducer.NO_CRUST_SANDWICH
                .createBreadToBeFilled();
        noCrustSandwichSpecial = FillingDecorator.BEEF_MEAT
                .addFillingToBread(noCrustSandwichSpecial);
        noCrustSandwichSpecial = FillingDecorator.TOMATO_SAUCE
                .addFillingToBread(noCrustSandwichSpecial);

        System.out.println("No Crust Sandwich Special with ingredients of : "
                + noCrustSandwichSpecial.getDescription() + " with the price of : $"
                + noCrustSandwichSpecial.cost());

    }
}
