package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;


public class CompositeDemo {
    public static void main(String[] args) {
        Company company = new Company();

        try {
            Employees ihsan = new Ceo("Ihsan", 200000.00);
            company.addEmployee(ihsan);

            Employees james = new Cto("James", 100000.00);
            company.addEmployee(james);

            Employees stanley = new BackendProgrammer("Stanley", 20000.00);
            company.addEmployee(stanley);

            Employees louis = new FrontendProgrammer("Louis", 30000.00);
            company.addEmployee(louis);

            Employees jamie = new SecurityExpert("Jamie", 70000);
            company.addEmployee(jamie);

            Employees owen = new NetworkExpert("Owen", 50000.00);
            company.addEmployee(owen);

            Employees amir = new UiUxDesigner("Amir", 90000.00);
            company.addEmployee(amir);

            //below minimum salary
            Employees cole = new BackendProgrammer("Cole", 5000.00);
            company.addEmployee(cole);

        } catch (IllegalArgumentException e) {
            e.getStackTrace();
        }

        for (Employees employee : company.employeesList) {
            System.out.println(employee.getName() + " as "
                    + employee.getRole() + " with salary " + employee.getSalary());
        }

        System.out.println("Total salary to be paid by the company : "
                + company.getNetSalaries());


    }
}
